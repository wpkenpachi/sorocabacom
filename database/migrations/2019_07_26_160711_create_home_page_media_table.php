<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomePageMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_page_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('game_name');
            $table->text('game_phrase');
            $table->text('form_description');
            $table->string('background_image_path');
            $table->string('spotlight_theme_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_page_media');
    }
}
