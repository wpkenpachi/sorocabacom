$('#toggle-edit').on('change', function() {
    const status = !!$(this).is(':checked');
    console.log(status);
    if (status) {
        $('#toggle-label').text('Edit On');
        $('#btn-toggle-label').addClass('btn-primary');
        $('#btn-toggle-label').removeClass('btn-secondary');
    } else {
        $('#toggle-label').text('Edit Off');
        $('#btn-toggle-label').addClass('btn-secondary');
        $('#btn-toggle-label').removeClass('btn-primary');
    }
    changeFormMode(status);
});


function changeFormMode (status) {
    $("#game-name").attr('disabled', !status);
    $("#game-phrase").attr('disabled', !status);
    $('#form-decription').attr('disabled', !status);
    $("#spotlight-image").attr('disabled', !status);
    $("#background-image").attr('disabled', !status);
}

function readImgURL(input, imgId) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $(imgId).attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
}

$("#spotlight-image").change(function() {
    readImgURL(this, "#spotlight-image-src");
});

$("#background-image").change(function() {
    readImgURL(this, "#background-image-src");
});
