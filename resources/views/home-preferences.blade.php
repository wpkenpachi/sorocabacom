@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <form>
            <div class="btn-group-toggle" data-toggle="buttons">
                <label id="btn-toggle-label" class="btn btn-secondary">
                    <span id="toggle-label"> Edit Off </span>
                    <input id="toggle-edit" type="checkbox">
                </label>
            </div>
            <div class="form-group">
                <label for="game-name">Nome do Game</label>
                <input type="text" class="form-control" disabled id="game-name" value="{{ $data['game_name'] }}">
            </div>
            <div class="form-group">
                <label for="form-decription">Texto do Formulário</label>
                <input type="text" class="form-control" disabled id="form-decription" value="{{ $data['form_description'] }}">
            </div>
            <div class="form-group">
                <img id="spotlight-image-src" src="{{ $data['spotlight_theme_path'] }}" width="385px" height="535px" class="rounded mx-auto d-block" alt="...">
                <label for="spotlight-image">Imagem em Destaque</label>
                <input type="file" class="form-control-file" disabled id="spotlight-image">
            </div>
            <div class="form-group">
                <label for="game-phrase">Frase em Destaque</label>
                <input type="text" class="form-control" disabled id="game-phrase" value="{{ $data['game_phrase'] }}">
            </div>
            <div class="form-group">
                <img id="background-image-src" src="{{ $data['background_image_path'] }}" height="385px" width="535px" class="rounded mx-auto d-block" alt="...">
                <label for="background-image">Imagem de Background</label>
                <input type="file" class="form-control-file" disabled id="background-image">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-block btn-primary"> Salvar </button>
            </div>
        </form>
    </div>
</div>
@endsection