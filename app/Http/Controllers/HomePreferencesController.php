<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HomePageMedia as HomePage;

class HomePreferencesController extends Controller
{
    public function index() {
        $data['data'] = [
            'game_name'             => 'Transistor',
            'game_phrase'           => '"Olha, o que quer que você esteja pensando, me faça um favor, não solte."',
            'form_description'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'spotlight_theme_path'  => 'https://public-v2links.adobecc.com/ae3a652f-3a94-4950-7008-6c9e9d27ab1f/component?params=component_id:c0778aba-4906-4c4d-b1e2-9bd1df6cdcb2&params=version:0&token=1564242577_da39a3ee_70d516b109755eedf458ea036b6a5de007422cba&api_key=CometServer1',
            'background_image_path' => 'https://public-v2links.adobecc.com/ae3a652f-3a94-4950-7008-6c9e9d27ab1f/component?params=component_id:4f6b1ff4-8d43-4b27-af53-3c87ce9e021e&params=version:0&token=1564242577_da39a3ee_70d516b109755eedf458ea036b6a5de007422cba&api_key=CometServer1'
        ];//HomePage::first()->toArray();
        return view('home-preferences', $data);
    }
}
